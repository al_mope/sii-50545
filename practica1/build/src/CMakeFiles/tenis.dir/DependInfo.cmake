# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/alvaro/sii-50545/practica1/src/Esfera.cpp" "/home/alvaro/sii-50545/practica1/build/src/CMakeFiles/tenis.dir/Esfera.cpp.o"
  "/home/alvaro/sii-50545/practica1/src/Mundo.cpp" "/home/alvaro/sii-50545/practica1/build/src/CMakeFiles/tenis.dir/Mundo.cpp.o"
  "/home/alvaro/sii-50545/practica1/src/Plano.cpp" "/home/alvaro/sii-50545/practica1/build/src/CMakeFiles/tenis.dir/Plano.cpp.o"
  "/home/alvaro/sii-50545/practica1/src/Raqueta.cpp" "/home/alvaro/sii-50545/practica1/build/src/CMakeFiles/tenis.dir/Raqueta.cpp.o"
  "/home/alvaro/sii-50545/practica1/src/Vector2D.cpp" "/home/alvaro/sii-50545/practica1/build/src/CMakeFiles/tenis.dir/Vector2D.cpp.o"
  "/home/alvaro/sii-50545/practica1/src/tenis.cpp" "/home/alvaro/sii-50545/practica1/build/src/CMakeFiles/tenis.dir/tenis.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
